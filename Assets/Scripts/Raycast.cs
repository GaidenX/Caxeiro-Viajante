﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Raycast : MonoBehaviour
{
    int count = 0;
    public Vector3 positionObj;
    public string primeiroSelect = "";
    public string segundoSelect = "";

    // obj's game
    private GameObject caxeiroEsq, alfaceEsq, cabraEsq, oncaEsq;
    private GameObject caxeiroDir, alfaceDir, cabraDir, oncaDir;
    private Vector3 caxeiroEsqPos, alfaceEsqPos, cabraEsqPos, oncaEsqPos;
    private Vector3 caxeiroDirPos, alfaceDirPos, cabraDirPos, oncaDirPos;
    private Estados estadoAtual;

    //definicão dos lados do game
    public GameObject ladoEsquerdo, ladoDireito;
    public bool startEsquerdo = true;

    // UI
    public GameObject ButtonObj;

    // atravessar
    public bool rioDireito = false;
    private GameObject atravessarPrimeiro;
    private GameObject atravessarSegundo;
    private bool atravessarChamada = false;

    void Start()
    {
        // achar os objetos
        caxeiroEsq = GameObject.Find("caxeiroEsq");
        caxeiroDir = GameObject.Find("caxeiroDir");
        alfaceEsq = GameObject.Find("alfaceEsq");
        alfaceDir = GameObject.Find("alfaceDir");
        cabraEsq = GameObject.Find("cabraEsq");
        cabraDir = GameObject.Find("cabraDir");
        oncaEsq = GameObject.Find("oncaEsq");
        oncaDir = GameObject.Find("oncaDir");

        // guardar a posicao inicial dos objetos
        caxeiroEsqPos = caxeiroEsq.transform.position;
        alfaceEsqPos = alfaceEsq.transform.position;
        cabraEsqPos = cabraEsq.transform.position;
        oncaEsqPos = oncaEsq.transform.position;
        caxeiroDirPos = caxeiroDir.transform.position;
        alfaceDirPos = alfaceDir.transform.position;
        cabraDirPos = cabraDir.transform.position;
        oncaDirPos = oncaDir.transform.position;

        ButtonObj = GameObject.Find("atravessar");

        ladoEsquerdo.gameObject.SetActive(true);
        ladoDireito.gameObject.SetActive(false);

        StartCoroutine(UpdateEstados());
    }

    // funcoes dos botoes da esquerda
    public void SelectCaxeiroEsq()
    {
        if (count < 2)
        {
            caxeiroEsq.transform.position = new Vector3((positionObj.x) + 4, positionObj.y, positionObj.z);
            count++;
            //Debug.Log("Qtd Objetos no barco: " + count);
            //Debug.Log("Nome Objetos no barco: " + caxeiro);
            AddBarco(1);
        }
    }

    public void SelectAlfaceEsq()
    {
        if (count < 2)
        {
            alfaceEsq.transform.position = new Vector3(positionObj.x, positionObj.y, positionObj.z);
            count++;
            AddBarco(2);
        }
    }

    public void SelectCabraEsq()
    {
        if (count < 2)
        {
            cabraEsq.transform.position = new Vector3(positionObj.x, positionObj.y, positionObj.z);
            count++;
            AddBarco(3);
        }
    }

    public void SelectOncaEsq()
    {
        if (count < 2)
        {
            oncaEsq.transform.position = new Vector3(positionObj.x, positionObj.y, positionObj.z);
            count++;
            AddBarco(4);
        }
    }


    // funcoes dos botoes da direita
    public void SelectCaxeiroDir()
    {
        if (count < 2)
        {
            caxeiroDir.transform.position = new Vector3((positionObj.x) + 4, positionObj.y, positionObj.z);
            count++;
            //Debug.Log("Qtd Objetos no barco: " + count);
            //Debug.Log("Nome Objetos no barco: " + caxeiro);
            AddBarco(1);
        }
    }

    public void SelectAlfaceDir()
    {
        if (count < 2)
        {
            alfaceDir.transform.position = new Vector3(positionObj.x, positionObj.y, positionObj.z);
            count++;
            AddBarco(2);
        }
    }

    public void SelectCabraDir()
    {
        if (count < 2)
        {
            cabraDir.transform.position = new Vector3(positionObj.x, positionObj.y, positionObj.z);
            count++;
            AddBarco(3);
        }
    }

    public void SelectOncaDir()
    {
        if (count < 2)
        {
            oncaDir.transform.position = new Vector3(positionObj.x, positionObj.y, positionObj.z);
            count++;
            AddBarco(4);
        }
    }


    public void AddBarco(int objValor)
    {
        // adicao caxeiro
        if (objValor == 1 && count <= 2)
        {
            if (primeiroSelect == "")
            {
                primeiroSelect = "caxeiro";
                Debug.Log("adicionado no primeiro: " + primeiroSelect);
            }
            else if (segundoSelect == "") {
                segundoSelect = "caxeiro";
                Debug.Log("adicionado no segundo: " + segundoSelect);
            }
        }

        // adicao alface
        if (objValor == 2 && count <= 2)
        {
            if (primeiroSelect == "")
            {
                primeiroSelect = "alface";
                Debug.Log("adicionado no primeiro: " + primeiroSelect);
            }
            else if (segundoSelect == "")
            {
                segundoSelect = "alface";
                Debug.Log("adicionado no segundo: " + segundoSelect);
            }
        }

        // adicao cabra
        if (objValor == 3 && count <= 2)
        {
            if (primeiroSelect == "")
            {
                primeiroSelect = "cabra";
                Debug.Log("adicionado no primeiro: " + primeiroSelect);
            }
            else if (segundoSelect == "")
            {
                segundoSelect = "cabra";
                Debug.Log("adicionado no segundo: " + segundoSelect);
            }
        }

        // adicao onca
        if (objValor == 4 && count <= 2)
        {
            if (primeiroSelect == "")
            {
                primeiroSelect = "onca";
                Debug.Log("adicionado no primeiro: " + primeiroSelect);
            }
            else if (segundoSelect == "")
            {
                segundoSelect = "onca";
                Debug.Log("adicionado no segundo: " + segundoSelect);
            }
        }
    }

    public void atravessarOn() {
        if (count >= 1 && count < 3) {
            atravessarChamada = true;
        }
    }

    public enum Estados
    {
        Atravessar,
        Checar,
        VitoriaDerrota
    }

    IEnumerator UpdateEstados() {
        while (true) {
            estadoAtual = Estados.Checar;
            checarObjetos();

            if (atravessarChamada) {
                estadoAtual = Estados.Atravessar;
                atravessarBarco();
            }

            yield return new WaitForSeconds(2f);
        }
    }

    void checarObjetos() {
        //caxeiros
        if (caxeiroEsq.gameObject.activeInHierarchy) {
            caxeiroDir.gameObject.SetActive(false);
        }
        if (caxeiroEsq.gameObject.activeInHierarchy == false)
        {
            caxeiroDir.gameObject.SetActive(true);
        }

        //alface
        if (alfaceEsq.gameObject.activeInHierarchy)
        {
            alfaceDir.gameObject.SetActive(false);
        }
        if (alfaceEsq.gameObject.activeInHierarchy == false)
        {
            alfaceDir.gameObject.SetActive(true);
        }

        //cabra
        if (cabraEsq.gameObject.activeInHierarchy)
        {
            cabraDir.gameObject.SetActive(false);
        }
        if (cabraEsq.gameObject.activeInHierarchy == false)
        {
            cabraDir.gameObject.SetActive(true);
        }

        //onca
        if (oncaEsq.gameObject.activeInHierarchy)
        {
            oncaDir.gameObject.SetActive(false);
        }
        if (oncaEsq.gameObject.activeInHierarchy == false)
        {
            oncaDir.gameObject.SetActive(true);
        }

        estadoAtual = Estados.VitoriaDerrota;
        VitoriaDerrota();

    }

    void atravessarBarco() {
        Debug.Log("Mudando de estado");

        // verificar se o caxeiro foi colocado

        if (primeiroSelect != "caxeiro" && segundoSelect != "caxeiro")
        {
            Debug.Log("Incapaz de prosseguir");
        }
        else { 

        // se o lado direito esta falso vamos verificar e transferir os objetos
        if (rioDireito == false) {
            ladoEsquerdo.gameObject.SetActive(false);
            ladoDireito.gameObject.SetActive(true);
            if (primeiroSelect == "caxeiro") {
                primeiroSelect = "";
                caxeiroEsq.gameObject.SetActive(false);
                caxeiroDir.transform.position = caxeiroDirPos;
            }
            if (primeiroSelect == "alface")
            {
                primeiroSelect = "";
                alfaceEsq.gameObject.SetActive(false);
                alfaceDir.transform.position = alfaceDirPos;
            }
            if (primeiroSelect == "cabra")
            {
                primeiroSelect = "";
                cabraEsq.gameObject.SetActive(false);
                cabraDir.transform.position = cabraDirPos;
            }
            if (primeiroSelect == "onca")
            {
                primeiroSelect = "";
                oncaEsq.gameObject.SetActive(false);
                oncaDir.transform.position = oncaDirPos;
            }
            if (segundoSelect == "caxeiro") {
                segundoSelect = "";
                caxeiroEsq.gameObject.SetActive(false);
                caxeiroDir.transform.position = caxeiroDirPos;
            }
            if (segundoSelect == "alface")
            {
                segundoSelect = "";
                alfaceEsq.gameObject.SetActive(false);
                alfaceDir.transform.position = alfaceDirPos;
            }
            if (segundoSelect == "cabra")
            {
                segundoSelect = "";
                cabraEsq.gameObject.SetActive(false);
                cabraDir.transform.position = cabraDirPos;
            }
            if (segundoSelect == "onca")
            {
                segundoSelect = "";
                oncaEsq.gameObject.SetActive(false);
                oncaDir.transform.position = oncaDirPos;
            }

            rioDireito = true;
        }

        // JOGADA PARA O LADO ESQUERDO
        else
        {
            ladoEsquerdo.gameObject.SetActive(true);
            ladoDireito.gameObject.SetActive(false);
            if (primeiroSelect == "caxeiro")
            {
                primeiroSelect = "";
                caxeiroEsq.gameObject.SetActive(true);
                caxeiroEsq.transform.position = caxeiroEsqPos;
            }
            if (primeiroSelect == "alface")
            {
                primeiroSelect = "";
                alfaceEsq.gameObject.SetActive(true);
                alfaceEsq.transform.position = alfaceEsqPos;
            }
            if (primeiroSelect == "cabra")
            {
                primeiroSelect = "";
                cabraEsq.gameObject.SetActive(true);
                cabraEsq.transform.position = cabraEsqPos;
            }
            if (primeiroSelect == "onca")
            {
                primeiroSelect = "";
                oncaEsq.gameObject.SetActive(true);
                oncaEsq.transform.position = oncaEsqPos;
            }
            if (segundoSelect == "caxeiro")
            {
                segundoSelect = "";
                caxeiroEsq.gameObject.SetActive(true);
                caxeiroEsq.transform.position = caxeiroEsqPos;
            }
            if (segundoSelect == "alface")
            {
                segundoSelect = "";
                alfaceEsq.gameObject.SetActive(true);
                alfaceEsq.transform.position = alfaceEsqPos;
            }
            if (segundoSelect == "cabra")
            {
                segundoSelect = "";
                cabraEsq.gameObject.SetActive(true);
                cabraEsq.transform.position = cabraEsqPos;
            }
            if (segundoSelect == "onca")
            {
                segundoSelect = "";
                oncaEsq.gameObject.SetActive(true);
                oncaEsq.transform.position = oncaEsqPos;
            }

            rioDireito = false;
        }

        // reseta a travessia e volta o estado antigo
        atravessarChamada = false;
        count = 0;
        }
    }

    void VitoriaDerrota() {
        // Checagem lado Esquerdo - Derrota
        if (caxeiroEsq.gameObject.activeInHierarchy == false && alfaceEsq.gameObject.activeInHierarchy == true && cabraEsq.gameObject.activeInHierarchy == true && oncaEsq.gameObject.activeInHierarchy == false)
        {
            Debug.Log("Perdeu");
        }
        if (caxeiroEsq.gameObject.activeInHierarchy == false && cabraEsq.gameObject.activeInHierarchy == true && oncaEsq.gameObject.activeInHierarchy == true && alfaceEsq.gameObject.activeInHierarchy == false)
        {
            Debug.Log("Perdeu");
        }

        // Checagem lado Direito - Derrota
        if (caxeiroDir.gameObject.activeInHierarchy == false && alfaceDir.gameObject.activeInHierarchy == true && cabraDir.gameObject.activeInHierarchy == true && oncaDir.gameObject.activeInHierarchy == false)
        {
            Debug.Log("Perdeu");
        }
        if (caxeiroDir.gameObject.activeInHierarchy == false && cabraDir.gameObject.activeInHierarchy == true && oncaDir.gameObject.activeInHierarchy == true && alfaceDir.gameObject.activeInHierarchy == false)
        {
            Debug.Log("Perdeu");
        }

        // Checagem lado Direito - Vitoria
        if (cabraDir.gameObject.activeInHierarchy == true && oncaDir.gameObject.activeInHierarchy == true && alfaceDir.gameObject.activeInHierarchy == true && caxeiroDir.gameObject.activeInHierarchy == true)
        {
            Debug.Log("Vitoria");
        }
    }

}
